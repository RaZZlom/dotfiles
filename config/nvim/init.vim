call plug#begin('~/.vim/plugged')

Plug 'https://github.com/morhetz/gruvbox.git'
Plug 'https://github.com/jiangmiao/auto-pairs.git'
Plug 'https://github.com/Yggdroot/indentLine.git'
Plug 'https://github.com/vim-scripts/vim-auto-save.git'
Plug 'https://github.com/dense-analysis/ale.git'
Plug 'https://github.com/junegunn/fzf.git', { 'do': { -> fzf#install() } }
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/habamax/vim-asciidoctor.git'
Plug 'https://github.com/ms-jpq/coq_nvim.git', { 'branch': 'coq' }
Plug 'https://github.com/vimwiki/vimwiki.git'

call plug#end()

" Plugin  indentLine config
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_conceallevel = 0

" Plugin AutoSave config
let g:auto_save = 1  " enable AutoSave on Vim startup"
let g:auto_save_in_insert_mode = 0  " do not save while in insert mode"

" Plugin ALE config
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_enter = 0
let g:ale_fix_on_save = 1

let g:ale_linters = {'python': ['pylint', 'mypy']}
let g:ale_fixers = {'*': ['trim_whitespace'], 'python': ['autopep8', 'isort'], 'pascal': ['ptop']}

let g:ale_python_mypy_options = '--strict'
let g:ale_pascal_ptop_options = '-c /home/razzlom/.config/ptop/ptop.cfg'

" Plugin fzf config
nnoremap <C-f> :Files<Cr>
nnoremap <C-b> :Buffers<Cr>

" Plugin coq config
" 'auto_start': 'shut-up' - auto start without notification
" 'display.icons.mode': 'none' - disable nerd font icons
" 'clients.snippets.warn': [] - disable warnings snippets not installed
let g:coq_settings = { 'auto_start': 'shut-up', 'display.icons.mode': 'none', 'clients.snippets.warn': [] }

" Plugin vimwiki config
" 'vimwiki_conceallevel' - do not hide markers around words and don't short links.
let g:vimwiki_conceallevel = 0
let g:vimwiki_conceal_onechar_markers = 0

" Vim config
syntax on
filetype plugin indent on
set number
set colorcolumn=80
set clipboard=unnamedplus " Use system clipboard (don't forget install xclip or xsel)
colorscheme gruvbox " Theme
set background=dark " Setting dark mode
set encoding=utf-8

" Russian words
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

" Spelling
set spell
set spelllang=ru_yo,en_us

au BufNewFile,BufRead *.py set filetype=python
au BufNewFile,BufRead *.py set tabstop=4
au BufNewFile,BufRead *.py set softtabstop=4
au BufNewFile,BufRead *.py set shiftwidth=4
au BufNewFile,BufRead *.py set expandtab

au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead *.md set tabstop=2
au BufNewFile,BufRead *.md set softtabstop=2
au BufNewFile,BufRead *.md set shiftwidth=2
au BufNewFile,BufRead *.md set expandtab

