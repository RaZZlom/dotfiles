-- Function to install pckr package manager to system
local function bootstrap_pckr()
    local pckr_path = vim.fn.stdpath("data") .. "/pckr/pckr.nvim"

    if not vim.loop.fs_stat(pckr_path) then
        vim.fn.system(
            {
                "git",
                "clone",
                "--filter=blob:none",
                "https://github.com/lewis6991/pckr.nvim",
                pckr_path
            }
        )
    end

    vim.opt.rtp:prepend(pckr_path)
end

-- Install pckr
bootstrap_pckr()

-- Plugins
require("pckr").add {
    "https://github.com/ellisonleao/gruvbox.nvim.git",
    "https://github.com/nvim-treesitter/nvim-treesitter.git",
    "https://github.com/neovim/nvim-lspconfig.git",
    "https://github.com/hrsh7th/nvim-cmp.git",
    "https://github.com/hrsh7th/cmp-nvim-lsp.git",
    "https://github.com/hrsh7th/cmp-buffer.git",
    "https://github.com/hrsh7th/cmp-path.git",
    "https://github.com/hrsh7th/cmp-cmdline.git",
    "https://github.com/hrsh7th/vim-vsnip.git",
    "https://github.com/hrsh7th/cmp-vsnip.git",
    { "https://github.com/nvim-telescope/telescope.nvim.git",
        tag = "0.1.8",
        requires = { { "https://github.com/nvim-lua/plenary.nvim.git" } } },
    { "https://github.com/ZhiyuanLck/smart-pairs.git",
        event = "InsertEnter",
        config = function() require("pairs"):setup() end },
    { "https://github.com/okuuva/auto-save.nvim.git",
        config = function() require("auto-save").setup {} end },
    { "https://github.com/lukas-reineke/indent-blankline.nvim.git",
        config = function() require("ibl").setup() end }
}

-- nvim-treesitter config
require "nvim-treesitter.configs".setup {
    ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "python" }, -- A list of parser names, or "all"
    sync_install = false,                                                  -- Install parsers synchronously (only applied to `ensure_installed`)
    ignore_install = {},                                                   -- List of parsers to ignore installing (or "all")
    auto_install = true,                                                   -- Automatically install missing parsers when entering buffer
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false, -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    }
}

-- nvim-lspconfig config
require "lspconfig".lua_ls.setup {
    settings = {
        Lua = {
            format = {
                enable = true,
                defaultConfig = {
                    indent_style = "space",
                    indent_size = "2",
                }
            },
            diagnostics = {
                globals = { "vim" }, -- Get the language server to recognize the `vim` global
            },
            workspace = {
                library = vim.api.nvim_get_runtime_file("", true), -- Make the server aware of Neovim runtime files
            },

        },
    }
}


require "lspconfig".pylsp.setup {
    settings = {
        pylsp = {
            plugins = {
                -- formatter options
                black = { enabled = true },
                autopep8 = { enabled = false },
                yapf = { enabled = false },
                -- linter options
                pylint = { enabled = true, executable = "pylint" },
                pyflakes = { enabled = false },
                pycodestyle = { enabled = false },
                -- type checker
                pylsp_mypy = { enabled = true, strict = true },
                -- auto-completion options
                jedi_completion = { fuzzy = true },
                -- import sorting
                pyls_isort = { enabled = true },
            },
        },
    },
}

vim.cmd [[autocmd BufWritePre * lua vim.lsp.buf.format()]] -- Format code on save

-- nvim-cmp config
require "cmp".setup(
    {
        snippet = {
            expand = function(args)
                vim.fn["vsnip#anonymous"](args.body)
            end
        },
        mapping = require "cmp".mapping.preset.insert(
            {
                ["<Tab>"] = require "cmp".mapping.select_next_item(),
                ["<S-Tab>"] = require "cmp".mapping.select_prev_item(),
                ["<CR>"] = require "cmp".mapping.confirm({ select = true })
            }
        ),
        sources = require "cmp".config.sources(
            {
                { name = "nvim_lsp" },
                { name = "vsnip" }
            },
            {
                { name = "buffer" }
            }
        )
    }
)

-- telescope config
vim.keymap.set("n", "<C-f>", require("telescope.builtin").find_files, {})
vim.keymap.set("n", "<C-b>", require("telescope.builtin").buffers, {})

-- Nvim config
vim.cmd.colorscheme("gruvbox")       -- Choose nvim theme
vim.opt.background = "dark"          -- Change theme color, can be "light" or "dark"
vim.opt.number = true                -- Line numbers
vim.opt.colorcolumn = "80"           -- Vertical line at 80's character
vim.opt.clipboard = "unnamedplus"    -- Use system clipboard (don't forget to install xclip or xsel)
vim.opt.encoding = "utf-8"           -- UTF-8 - default file encoding
vim.opt.cursorline = true            -- Highlight the current line
vim.opt.tabstop = 4                  -- Number of spaces a tab represents
vim.opt.softtabstop = 4              -- Number of spaces a tab represents
vim.opt.shiftwidth = 4               -- Number of spaces for each indentation
vim.opt.expandtab = true             -- Convert tabs to spaces
vim.opt.smartindent = true           -- Automatically indent new lines
vim.opt.keymap = "russian-jcukenwin" -- Now you can write with Russian keyboard layout (Ctrl + Shift + 6)
vim.opt.iminsert = 0
vim.opt.imsearch = 0
vim.opt.spell = true                     -- Enable spellchecking
vim.opt.spelllang = { "ru_yo", "en_us" } -- Spellcheck Russian and English words
